#include "proto/visor.pb.h"

#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <fcntl.h>
#include <unistd.h>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>

using google::protobuf::io::FileInputStream;
using google::protobuf::io::ZeroCopyInputStream;
using google::protobuf::io::CodedInputStream;

void read_model_from_textfile(const std::string& input_model,
                              std::string& id,
                              std::vector<float>& model) {

  std::cout << "Getting model data..." << std::endl;

  {
    std::ifstream infile(input_model);

    std::getline(infile, id);

    std::string line;
    while(std::getline(infile,line)) {
      std::istringstream iss(line);
      float value;
      iss >> value;
      model.push_back(value);
    }
  }

  std::cout << "Read in data for class: " << id << std::endl;
  std::cout << "Model of length: " << model.size() << std::endl;
}

void save_proto(const std::string& proto_path,
                const std::string& id,
                const std::vector<float>& model) {

  std::cout << "Preparing protobuf..." << std::endl;

  ModelProto model_proto;
  model_proto.set_id(id);
  model_proto.set_dim(model.size());
  model_proto.clear_data();

  for (auto dim : model) {
    model_proto.add_data(dim);
  }

  std::cout << "Saving to file..." << std::endl;
  std::fstream output(proto_path.c_str(),
                      std::ios::out | std::ios::trunc | std::ios::binary);
  assert(model_proto.SerializeToOstream(&output));
}

bool verify_proto(const std::string& proto_path,
                  const std::string& id,
                  const std::vector<float>& model) {

  std::cout << "Verifying file..." << std::endl;

  ModelProto model_proto;

  {
    int fd = open(proto_path.c_str(), O_RDONLY);

    std::shared_ptr<ZeroCopyInputStream> raw_input(new FileInputStream(fd));
    std::shared_ptr<CodedInputStream> coded_input(new CodedInputStream(raw_input.get()));
    coded_input->SetTotalBytesLimit(1073741824, 536870912);

    assert(model_proto.ParseFromCodedStream(coded_input.get()));

    close(fd);
  }

  // verify
  if (model_proto.dim() != model.size()) return false;

  if (model_proto.id() != id) return false;

  for (size_t i = 0; i < model_proto.dim(); ++i) {
    if (model_proto.data(i) != model[i]) return false;
  }

  return true;
}

// -----------------------------------------------------------------

int main(int argc, char* argv[]) {
  std::string input_model = "example_text_model.txt";
  std::string proto_path = "test.binaryproto";

  std::string id;
  std::vector<float> model;

  read_model_from_textfile(input_model, id, model);
  save_proto(proto_path, id, model);
  assert(verify_proto(proto_path, id, model));


}
