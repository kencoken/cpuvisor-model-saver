CXX=g++
CXXFLAGS=-std=c++11
LIBS=-lprotobuf
BUILD_DIR=build

.PHONY: all
all: save_model
	
save_model: $(BUILD_DIR)/save_model.o $(BUILD_DIR)/visor.pb.o
	$(CXX) $(LIBS) -o  $@ $^
	
$(BUILD_DIR)/save_model.o: src/proto/visor.pb.cc
	$(CXX) $(CXXFLAGS) -c -o $@ src/save_model.cc
	
$(BUILD_DIR)/visor.pb.o: src/proto/visor.pb.cc
	$(CXX) $(CXXFLAGS) -c -o $@ $^
	
src/proto/visor.pb.cc: src/proto/visor.proto $(BUILD_DIR)
	protoc --proto_path=src/proto --cpp_out=src/proto src/proto/visor.proto
	
$(BUILD_DIR):
	mkdir -p $@

.PHONY: clean
clean:
	-rm -rf build
	-rm src/proto/*.pb.*
	-rm save_model
