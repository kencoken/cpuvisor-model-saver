Example of saving CPUVISOR compatible models
--------------------------------------------

Dependencies:

  * `protobuf` library (`sudo apt-get install protobuf`, `brew install protobuf` etc.)

Usage:

    $ make
    $ ./save_model

What it does:

  * Read in the model defined as a textfile in `example_text_model.txt`
  * Save it to `test.binaryproto`, which is a cpuvisor-srv compatible model file
  * Verify the model has been saved correctly (can remove in custom saving code)

Edit `src/save_model.cc` to create custom model saving code.
